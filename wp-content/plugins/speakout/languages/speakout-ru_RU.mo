��    p      �  �         p	     q	     �	     �	  G   �	     �	     �	     �	     �	     
     	
     
     
     $
     8
  	   R
     \
     d
     q
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
          $     +     ;     ?     D     J     X  
   k     v     �     �     �     �     �  
   �     �     �     �     �     �     �     �     �     �  	   �                                         #     (     +     4     9     =     A  	   H  �   R  !   �  T        r  
   z     �     �     �     �     �     �  
   �  
   �     �     �                    *     :  	   B     L     R  	   X     b     r  	   v     �     �     �     �     �     �     �     �     �     �            4   
     ?  I  N     �  &   �     �  �   �     �     �     �     �     �  
   �     �      �  l     m   y     �       )     !   ;     ]     f     �  C   �     �     �     �  !   �  8   !  >   Z     �     �     �     �               7     =  <   C  
   �  4   �     �     �     �  ,        9     @     G     T     k     �     �     �     �     �     �     �  
   �     �     �     �     �          	          #     *     D     Q     X  
   _     j  \  �  _   �  �   ?  $   �          -  %   D     j     q  &   �  2   �     �     �     �       
   )  
   4  (   ?  =   h     �     �     �     �     �  !         "     '     D     U  7   f     �     �     �  0   �  C   �  ,   (     U     q     �  i   �     �     f             =   ?   ,         ^   o       ;       V      Q   A   C   \   E   )           .   W          P   O   X           	           %   @                 
   2   d   >          j   I          G       k   _      J   8                  4   M   5   (   l                 [      Z   n       :   "       *                        a      g               p   /   ]   7   3   S   #   e      R   h               D       K             L   -   N   1   b   U   '      `                H   m   0   !       &       +   9   F       <      Y   $   i   B   T       6   c       Accepted tags: Accepted variables: Add New Add a custom field to the petition form for collecting additional data. Apr Aug Basic Call to Action Checked City Columns Confirm Email Confirmation Emails Confirmation emails sent. Confirmed Country Custom Field Custom Message Date Date Signed Dec Default Delete Display Display BCC field Display Options Display custom field Display custom message Display honorific Display honorific field Donate Download as CSV Dr. Edit Email Email Address Email Confirmation Email From Email Message Email Opt-in Email Subject End date Enter title here Feb First Name Goal Greeting Hello ID Jan Jul Jun Label Language Last Name Mar May Message Miss Mr Mrs Ms Name No No Title None Nov Oct Opt-in Petition  Petition created. Use %1$s %2$s %3$s to display in a page or post. Use %1$s %4$s %3$s to display the signatures list or %1$s %5$s %3$s to show just the signature count. Please confirm your email address Please confirm your email address by clicking or copying and pasting the link below: Privacy Return URL Rows Save Changes Sep Settings Settings updated. Share this with your friends: Shortcodes Signatures State State / Province Street Street Address Submit Button Text Success Message Support Thank you Theme Title Unchecked View Signatures Yes confirmed disabled items long list (comma separated) no of of goal rows - default = 20 rows - default = 50 signature signatures unconfirmed use yes you may enter multiple addresses separated by commas your signature Project-Id-Version: SpeakOut! Email Petitions
POT-Creation-Date: 2018-02-22 11:07+1000
PO-Revision-Date: 2018-02-22 11:08+1000
Last-Translator: 123host <info@123host.com.au>
Language-Team: 
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 2.0.6
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: speakout-email-petitions.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Принятые теги: Принятых переменных: Добавить новое Добавьте настраиваемое поле в форму петицию для сбора дополнительных данных. Апр Авг главный Вызов действия Проверено город Колонки Подтверждение email Сколько нужно Писем Подтверждения для нескольких подписок Сколько нужно Писем Подтверждения для нескольких подписок. Подтвержденные Страна Пользовательское поле Сообщение клиента Дата Дата подписания Дек По умолчанию
Предложить исправление Удалить Дисплей Поле «СК» Показать варианты Отображать настраиваемое поле Отображать собственное сообщение Почетный дисплея Почетный поле Пожертвовать Скачать как .csv компания Dr. Редактировать Email Email Подтверждение электронной почты Email ОТ Сообщение электронной почты Емейл подписка Субъект E-mail Конечная дата Введите Заголовок здесь Фев Имя გოლი Приветствие Здравствуйте ID Янв Июль Июнь Этикетка Язык Фамилия Мартт Май Сообщение Миссис Г-н Г-жа Мисс Название Нет Без заголовка Ничего Нов Окт Форма Ходатайство  Петиция создан. Используйте %1$s %2$s %3$s для отображения на странице или должности. Используйте %1$s %4$s %3$s для отображения списка подписей или %1$s %5$s %3$s  чтобы отобразить только количество подписи. Пожалуйста подтвердите ваш адрес электронной почты Пожалуйста, подтвердите свой адрес электронной почты, нажав или скопировав и вставив ссылку ниже: Конфиденциальность URL возврата Вертикально Сохранить Изменения Сен Настройки Настройки обновлены. Расскажите об этом друзьям: Шорткоды Подписи Страна Штат / Провинция Улица Адрес Кнопка отправки Текст Сообщение об успешном завершении Поддержка Спасибо Вам Тема Заглавие Непроверено Просмотр подписей Да подтвержденный отключен предметы длинный список (через запятую) нет из цели По умолчанию: 20 капель / мл. По умолчанию
Предложить исправление подпись подпись подпись не подтвержден использовать да Вы можете ввести несколько адресов, разделенных запятыми ВАША ПОДПИСЬ 