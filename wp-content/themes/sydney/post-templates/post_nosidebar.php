<?php
/*
Template Name: No sidebar
Template Post Type: post, projects, employees
*/

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'content', 'single' );

			sydney_post_navigation();


		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
			<p style="margin: 20px 0">
				<a href="/#petition" class="roll-button button-slider scroll-link" data-target="petition">Sign the Petition!</a>
			</p>
	</div><!-- #primary -->

<?php
get_footer();
